<?php include("header.php"); ?>
<div class="notice-holder fixed">
   <div class="settings-notice">
      <span class="success-note"></span>
      <span class="error-note"></span>
      <span class="info-note"></span>
   </div>
</div>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div>
<?php include("common/leftmenu.php"); ?>
<div class="fixed-layout messages-page-fixed mt-0">
   <div class="main-content with-lmenu messages-page main-page">
      <div class="combined-column">
         <div class="content-box messages-list">
            <div class="cbox-desc">
               <div class="row">
                  <div class="left-section">
                     <div class="side-user">
                        <span class="img-holder"><a href="wall.html"><img class="circle" src="images/demo-profile.jpg" /></a></span>
                        <a href="wall.html"><span class="desc-holder">Nimish Parekh</span></a>
                        <span class="online-dot"></span><span class="userstatus">Online</span>
                     </div>
                     <div class="connections-search">
                        <div class="fsearch-form">
                           <input type="text" placeholder="Search"/>
                           <a href="javascript:void(0)"><i class="zmdi zmdi-search"></i></a>
                           <a href="javascript:void(0)" id="addnewchat" class="addnewchat"><i class="mdi mdi-pencil-square-o"></i></a>
                        </div>
                     </div>
                     <div class="clear"></div>
                     <div class="fake-title-area">
                        <ul class="nav nav-tabs">
                           <li class="active"><a href="#messages-inbox" data-toggle="tab" aria-expanded="false">Inbox</a></li>
                           <li><a href="#messages-connect" data-toggle="tab" aria-expanded="false">Requests</a></li>
                           <li><a href="#messages-spam" data-toggle="tab" aria-expanded="false">Other</a></li>
                        </ul>
                     </div>
                     <div class="tab-content">
                        <div class="tab-pane fade main-pane active in" id="messages-inbox">
                           <div class="gloader-holder doneloading">
                              <div class="gloader-content">
                                 <div class="message-userlist nice-scroll">
                                    <ul>
                                       <li>
                                          <a href="javascript:void(0)" class="active" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder"><img src="images/whoisaround-img.png"/></span>
                                                <span class="descholder">
                                                   <h6>Adel Hasanat</h6>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   <span class="newmsg-count">2</span>
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user2">
                                             <span class="muser-holder">
                                                <span class="imgholder"><img src="images/whoisaround-img1.png"/></span>
                                                <span class="descholder">
                                                   <h6>Bhadresh Ramani</h6>
                                                   <p><img class="msg-status" src="images/msg-sent.png" /><i class="mdi mdi-reply"></i> hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user3">
                                             <span class="muser-holder">
                                                <span class="imgholder">
                                                <img src="images/whoisaround-img.png"/>
                                                </span>
                                                <span class="descholder">
                                                   <h6>Adel Hasanat</h6>
                                                   <p><img class="msg-status" src="images/msg-unread.png" /> <i class="mdi mdi-gift"></i> You sent a gift</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user4">
                                             <span class="muser-holder">
                                                <span class="imgholder"><img src="images/whoisaround-img.png"/></span>
                                                <span class="descholder">
                                                   <h6>Adel Hasanat</h6>
                                                   <p><img class="msg-status" src="images/msg-read.png" /><i class="mdi mdi-file-image"></i> You sent a photo</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user5">
                                             <span class="muser-holder">
                                                <span class="imgholder">
                                                <img src="images/whoisaround-img.png"/>
                                                </span>
                                                <span class="descholder">
                                                   <h6>Adel Hasanat</h6>
                                                   <p><i class="mdi mdi-file-image"></i> You received a photo</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user6">
                                             <span class="muser-holder">
                                                <span class="imgholder"><img src="images/whoisaround-img.png"/></span>
                                                <span class="descholder">
                                                   <h6>Adel Hasanat</h6>
                                                   <p>Social Networking Site</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user7">
                                             <span class="muser-holder">
                                                <span class="imgholder"><img src="images/whoisaround-img.png"/></span>
                                                <span class="descholder">
                                                   <h6>Adel Hasanat</h6>
                                                   <p>Social Networking Site</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user8">
                                             <span class="muser-holder">
                                                <span class="imgholder">
                                                <img src="images/whoisaround-img.png"/>
                                                </span>
                                                <span class="descholder">
                                                   <h6>Adel Hasanat</h6>
                                                   <p>Social Networking Site</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user8">
                                             <span class="muser-holder">
                                                <span class="imgholder">
                                                <img src="images/whoisaround-img.png"/>
                                                </span>
                                                <span class="descholder">
                                                   <h6>Adel Hasanat</h6>
                                                   <p>Social Networking Site</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user8">
                                             <span class="muser-holder">
                                                <span class="imgholder">
                                                <img src="images/whoisaround-img.png"/>
                                                </span>
                                                <span class="descholder">
                                                   <h6>Adel Hasanat</h6>
                                                   <p>Social Networking Site</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user8">
                                             <span class="muser-holder">
                                                <span class="imgholder">
                                                <img src="images/whoisaround-img.png"/>
                                                </span>
                                                <span class="descholder">
                                                   <h6>Adel Hasanat</h6>
                                                   <p>Social Networking Site</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user8">
                                             <span class="muser-holder">
                                                <span class="imgholder">
                                                <img src="images/whoisaround-img.png"/>
                                                </span>
                                                <span class="descholder">
                                                   <h6>Adel Hasanat</h6>
                                                   <p>Social Networking Site</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user8">
                                             <span class="muser-holder">
                                                <span class="imgholder">
                                                <img src="images/whoisaround-img.png"/>
                                                </span>
                                                <span class="descholder">
                                                   <h6>Adel Hasanat</h6>
                                                   <p>Social Networking Site</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user8">
                                             <span class="muser-holder">
                                                <span class="imgholder">
                                                <img src="images/whoisaround-img.png"/>
                                                </span>
                                                <span class="descholder">
                                                   <h6>Adel Hasanat</h6>
                                                   <p>Social Networking Site</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user8">
                                             <span class="muser-holder">
                                                <span class="imgholder">
                                                <img src="images/whoisaround-img.png"/>
                                                </span>
                                                <span class="descholder">
                                                   <h6>Adel Hasanat</h6>
                                                   <p>Social Networking Site</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0);">show earlier message</a>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="tab-pane fade main-pane" id="messages-connect">
                           <div class="gloader-holder">
                              <div class="gloader-content">
                                 <div class="message-userlist nice-scroll">
                                    <ul>
                                       <li>
                                          <div class="no-listcontent">
                                             No New Messages
                                          </div>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="tab-pane fade main-pane" id="messages-spam">
                           <div class="gloader-holder doneloading">
                              <div class="gloader-content">
                                 <div class="message-userlist nice-scroll">
                                    <ul>
                                       <li>
                                          <div class="no-listcontent">
                                             No New Messages
                                          </div>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class='clear'></div>
                     </div>
                     <a href="javascript:void(0)" onclick="openNewMessagemobile(this)" id="addnewchat" class="addnewchat mobile-btn"><i class="mdi mdi-pencil-square-o"></i></a>
                  </div>
                  <div class="right-section">
                     <div class="topstuff">
                        <div class="msgwindow-name">
                           <div class="imgholder"><img src="images/whoisaround-img.png"/></div>
                           <span class="desc-holder">Adel Hasanat</span>
                           <span class="online-dot"></span>  
                           <span class="userstatus">i like nonsense it wakes up the brain cells</span>
                           <span class="usertime"> 12:57 PM </span> 
                           <span class="usercountry">| INDIA </span>
                        </div>
                        <div class="msgwindow-name msgwindow-group">
                           <div class="imgholder fourpersongroup">
                              <div class="group-person">
                                 <img src="images/whoisaround-img.png"/>
                              </div>
                              <div class="group-person">
                                 <img class="circle" src="images/demo-profile.jpg" />
                              </div>
                              <div class="group-person">
                                 <img class="circle" src="images/demo-profile.jpg" />
                              </div>
                              <div class="group-person">
                                 <img src="images/whoisaround-img.png"/>
                              </div>
                           </div>
                           <span class="desc-holder">
                           <span class="group-name-default">
                           <span class="group-name"> Adel Hasanat </span>
                           <a href="javascript:void(0)" id="edit-group-name" class="edit-group-name"><i class="mdi mdi-pencil" ></i></a> 
                           </span>
                           <span class="group-name-editor">
                           <input type="text" class="group-name-input" />
                           <a href="javascript:void(0)" id="edit-group-done" class="edit-group-done"><i class="zmdi zmdi-check" ></i></a>
                           <a href="javascript:void(0)" id="edit-group-cancle" class="edit-group-cancle"><i class="mdi mdi-close" ></i></a>
                           </span>
                           </span> 
                           <span class="group-status"><i class="mdi mdi-chevron-right" ></i>&nbsp; 2 participants</span>
                        </div>
                        <div class="connections-search">
                           <div class="dropdown dropdown-custom dropdown-connectlock">
                              <a href="javascript:void(0)" class="dropdown-toggle msg-btns" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <img src="images/dot-hori.png" />
                              </a>
                              <ul class="dropdown-menu">
                                 <li><a href="javascript:void(0)" onclick="showMsgPhotos()">View in photos in thread</a></li>
                                 <li><a href="javascript:void(0)" class="mute-setting" onclick="manageMuteConverasion('mute')">Mute thread</a></li>
                                 <li><a href="javascript:void(0)">Delete thread</a></li>
                                 <li><a href="javascript:void(0)" onclick="showMsgCheckbox()">Delete Messages</a></li>
                                 <li><a href="javascript:void(0)" class="block-setting" onclick="manageBlockConverasion('block')">Block Messages</a></li>
                                 <li>
                                    <div class="clear"></div>
                                    <hr>
                                    <div class="clear"></div>
                                 </li>
                                 <li>
                                    <div class="h-checkbox">
                                       <input id="test1" type="checkbox">
                                       <label>Sound notification</label>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="add-chat-search">
                           <label>To: </label>
                           <select class="select2 add-multi-connections" multiple="multiple">
                              <option value="AL">Bhadresh</option>
                              <option value="WY">Alap</option>
                              <option value="WY">Markand</option>
                              <option value="WY">Hiral</option>
                           </select>
                           <a href="javascript:void(0)" id="canceladdchat" class="canceladdchat"><i class="mdi mdi-close"></i></a>
                           <a href="javascript:void(0)" id="doneaddchat" class="btn btn-sm btn-primary doneaddchat right">Done</a>
                        </div>
                     </div>
                     <div class="main-msgwindow">
                        <h4 class="dateshower"></h4>
                        <div class="photos-thread">
                           <a href="javascript:void(0)" onclick="hideMsgPhotos()" class="backlink"><i class="mdi mdi-menu-left"></i> Back to conversation</a>
                           <div class="photos-area nice-scroll">
                              <div class="albums-grid images-container">
                                 <div class="row">
                                    <div class="grid-box">
                                       <div class="photo-box">
                                          <div class="imgholder">
                                             <figure>
                                                <a href="images/post-img1.jpg" data-size="1600x1600" data-med="images/post-img1.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="himg-box">
                                                <img class="himg" src="images/post-img1.jpg">
                                                </a>																		
                                             </figure>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="grid-box">
                                       <div class="photo-box">
                                          <div class="imgholder">
                                             <figure>
                                                <a href="images/album2.png" data-size="1600x1600" data-med="images/album2.png" data-med-size="1024x1024" data-author="Folkert Gorter" class="himg-box">
                                                <img class="himg" src="images/album2.png">
                                                </a>																			
                                             </figure>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="grid-box">
                                       <div class="photo-box">
                                          <div class="imgholder">
                                             <figure>
                                                <a href="images/post-img5.jpg" data-size="1600x1600" data-med="images/post-img5.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="vimg-box">
                                                <img class="vimg" src="images/post-img5.jpg">
                                                </a>
                                             </figure>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="grid-box">
                                       <div class="photo-box">
                                          <div class="imgholder">
                                             <figure>
                                                <a href="images/post-img4.jpg" data-size="1600x1600" data-med="images/post-img4.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="himg-box">
                                                <img class="himg" src="images/post-img4.jpg">
                                                </a>
                                             </figure>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="grid-box">
                                       <div class="photo-box">
                                          <div class="imgholder">
                                             <figure>
                                                <a href="images/album5.png" data-size="1600x1600" data-med="images/album5.png" data-med-size="1024x1024" data-author="Folkert Gorter" class="himg-box">
                                                <img class="himg" src="images/album5.png">
                                                </a>																			
                                             </figure>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="grid-box">
                                       <div class="photo-box">
                                          <div class="imgholder">
                                             <figure>
                                                <a href="images/post-img3.jpg" data-size="1600x1600" data-med="images/post-img3.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="vimg-box">
                                                <img class="vimg" src="images/post-img3.jpg">
                                                </a>
                                             </figure>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="grid-box">
                                       <div class="photo-box">
                                          <div class="imgholder">
                                             <figure>
                                                <a href="images/album7.png" data-size="1600x1600" data-med="images/album7.png" data-med-size="1024x1024" data-author="Folkert Gorter" class="Himg-box">
                                                <img class="Himg" src="images/album7.png">
                                                </a>
                                             </figure>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="allmsgs-holder">
                           <div class="msg-notice">
                              <div class="mute-notice">
                                 This conversation has been muted. All the push notifications will be turned off. <a href="javascript:void(0)" onclick="manageMuteConverasion('unmute')">Unmute</a>
                              </div>
                              <div class="block-notice">
                                 This conversation is blocked. <a href="javascript:void(0)" onclick="manageBlockConverasion('unblock')">Unblock</a>
                              </div>
                           </div>
                           <ul class="current-messages">
                              <li class="mainli active" id="li-user1">
                                 <div class="msgdetail-list nice-scroll">
                                    <div class="msglist-holder images-container">
                                       <ul class="outer">
                                          <li class="msgli received msg-income">
                                             <div class="checkbox-holder">
                                                <div class="h-checkbox entertosend msg-checkbox">
                                                   <input type="checkbox" id="test1">
                                                   <label>&nbsp;</label>
                                                </div>
                                             </div>
                                             <div class="msgdetail-box">
                                                <div class="imgholder"><img src="images/whoisaround-img.png"/></div>
                                                <div class="descholder">
                                                   <p>Hi, Adel How are you? <span class="timestamp">1:30 PM</span></p>
                                                   <p>I hope you’d have gone through my previous message. We’d like to discuss this project in details so that we can nullify your concerns. <span class="timestamp">1:30 PM</span></p>
                                                </div>
                                             </div>
                                          </li>
                                          <li class="date-divider"><span>31 Jan 2016</span></li>
                                          <li class="msgli received msg-income">
                                             <div class="checkbox-holder">
                                                <div class="h-checkbox entertosend msg-checkbox">
                                                   <input type="checkbox" id="test1">
                                                   <label>&nbsp;</label>
                                                </div>
                                             </div>
                                             <div class="msgdetail-box">
                                                <div class="imgholder"><img src="images/whoisaround-img.png"/></div>
                                                <div class="descholder">
                                                   <p>
                                                      Hi, Adel How are you?																
                                                      <br />
                                                      I hope you’d have gone through my previous message. We’d like to discuss this project in details so that we can nullify your concerns.
                                                      <span class="timestamp">1:30 PM</span>
                                                   </p>
                                                </div>
                                             </div>
                                          </li>
                                          <li class="msgli msg-outgoing">
                                             <div class="checkbox-holder">
                                                <div class="h-checkbox entertosend msg-checkbox">
                                                   <input type="checkbox" id="test1">
                                                   <label>&nbsp;</label>
                                                </div>
                                             </div>
                                             <div class="msgdetail-box">
                                                <div class="descholder">
                                                   <img class="msg-status" src="images/msg-read.png">
                                                   <span class="msg-img">
                                                      <figure>
                                                         <a href="images/msg2.jpg" data-size="1600x1600" data-med="images/msg2.jpg" data-med-size="1024x1024" data-author="Folkert Gorter"><img src="images/msg2.jpg"/></a>
                                                         <a href="javascript:void(0)" class="download"><i class="mdi mdi-download"></i></a>
                                                         <span class="timestamp">1:30 PM</span>
                                                      </figure>
                                                   </span>
                                                </div>
                                             </div>
                                          </li>
                                          <li class="msgli received msg-income">
                                             <div class="checkbox-holder">
                                                <div class="h-checkbox entertosend msg-checkbox">
                                                   <input type="checkbox" id="test1">
                                                   <label>&nbsp;</label>
                                                </div>
                                             </div>
                                             <div class="msgdetail-box">
                                                <div class="imgholder"><img src="images/whoisaround-img.png"/></div>
                                                <div class="descholder">
                                                   <p>
                                                      Hi, Adel How are you?																
                                                      <br />
                                                      I hope you’d have gone through my previous message. We’d like to discuss this project in details so that we can nullify your concerns.
                                                      <span class="timestamp">1:30 PM</span>
                                                   </p>
                                                </div>
                                             </div>
                                          </li>
                                          <li class="date-divider"><span>4 Mar 2016</span></li>
                                          <li class="msgli msg-outgoing">
                                             <div class="checkbox-holder">
                                                <div class="h-checkbox entertosend msg-checkbox">
                                                   <input type="checkbox" id="test1">
                                                   <label>&nbsp;</label>
                                                </div>
                                             </div>
                                             <div class="msgdetail-box">
                                                <div class="descholder">
                                                   <img class="msg-status" src="images/msg-read.png">
                                                   <span class="msg-img">
                                                      <figure><a href="images/msg1.jpg" data-size="1600x1600" data-med="images/msg1.jpg" data-med-size="1024x1024" data-author="Folkert Gorter"><img src="images/msg1.jpg"/></a>
                                                         <a href="javascript:void(0)" class="download"><i class="mdi mdi-download"></i></a>
                                                         <span class="timestamp">11:30 PM</span>
                                                      </figure>
                                                   </span>
                                                </div>
                                             </div>
                                          </li>
                                          <li class="msgli msg-outgoing">
                                             <div class="checkbox-holder">
                                                <div class="h-checkbox entertosend msg-checkbox">
                                                   <input type="checkbox" id="test1">
                                                   <label>&nbsp;</label>
                                                </div>
                                             </div>
                                             <div class="msgdetail-box">
                                                <div class="descholder">
                                                   <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                   <span class="gift-img">
                                                   <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                   <i class="mdi mdi-gift"></i>
                                                   </a>
                                                   </span>
                                                   <img class="msg-status" src="images/msg-read.png">
                                                </div>
                                             </div>
                                          </li>
                                          <li class="msgli msg-outgoing">
                                             <div class="checkbox-holder">
                                                <div class="h-checkbox entertosend msg-checkbox">
                                                   <input type="checkbox" id="test1">
                                                   <label>&nbsp;</label>
                                                </div>
                                             </div>
                                             <div class="msgdetail-box">
                                                <div class="descholder">
                                                   <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                   <span class="gift-img">
                                                   <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                   <i class="mdi mdi-gift"></i>
                                                   </a>
                                                   </span>
                                                   <img class="msg-status" src="images/msg-read.png">
                                                </div>
                                             </div>
                                          </li>
                                          <li class="msgli msg-outgoing">
                                             <div class="checkbox-holder">
                                                <div class="h-checkbox entertosend msg-checkbox">
                                                   <input type="checkbox" id="test1">
                                                   <label>&nbsp;</label>
                                                </div>
                                             </div>
                                             <div class="msgdetail-box">
                                                <div class="descholder">
                                                   <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                   <span class="gift-img">
                                                   <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                   <i class="mdi mdi-gift"></i>
                                                   </a>
                                                   </span>
                                                   <img class="msg-status" src="images/msg-read.png">
                                                </div>
                                             </div>
                                          </li>
                                          <li class="msgli msg-outgoing">
                                             <div class="checkbox-holder">
                                                <div class="h-checkbox entertosend msg-checkbox">
                                                   <input type="checkbox" id="test1">
                                                   <label>&nbsp;</label>
                                                </div>
                                             </div>
                                             <div class="msgdetail-box">
                                                <div class="descholder">
                                                   <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                   <span class="gift-img">
                                                   <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                   <i class="mdi mdi-gift"></i>
                                                   </a>
                                                   </span>
                                                   <img class="msg-status" src="images/msg-read.png">
                                                </div>
                                             </div>
                                          </li>
                                          <li class="msgli msg-outgoing">
                                             <div class="checkbox-holder">
                                                <div class="h-checkbox entertosend msg-checkbox">
                                                   <input type="checkbox" id="test1">
                                                   <label>&nbsp;</label>
                                                </div>
                                             </div>
                                             <div class="msgdetail-box">
                                                <div class="descholder">
                                                   <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                   <span class="gift-img">
                                                   <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                   <i class="mdi mdi-gift"></i>
                                                   </a>
                                                   </span>
                                                   <img class="msg-status" src="images/msg-read.png">
                                                </div>
                                             </div>
                                          </li>
                                          <li class="msgli msg-outgoing">
                                             <div class="checkbox-holder">
                                                <div class="h-checkbox entertosend msg-checkbox">
                                                   <input type="checkbox" id="test1">
                                                   <label>&nbsp;</label>
                                                </div>
                                             </div>
                                             <div class="msgdetail-box">
                                                <div class="descholder">
                                                   <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                   <span class="gift-img">
                                                   <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                   <i class="mdi mdi-gift"></i>
                                                   </a>
                                                   </span>
                                                   <img class="msg-status" src="images/msg-read.png">
                                                </div>
                                             </div>
                                          </li>
                                          <li class="msgli msg-outgoing">
                                             <div class="checkbox-holder">
                                                <div class="h-checkbox entertosend msg-checkbox">
                                                   <input type="checkbox" id="test1">
                                                   <label>&nbsp;</label>
                                                </div>
                                             </div>
                                             <div class="msgdetail-box">
                                                <div class="descholder">
                                                   <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                   <span class="gift-img">
                                                   <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                   <i class="mdi mdi-gift"></i>
                                                   </a>
                                                   </span>
                                                   <img class="msg-status" src="images/msg-read.png">
                                                </div>
                                             </div>
                                          </li>
                                          <li class="msgli msg-outgoing">
                                             <div class="checkbox-holder">
                                                <div class="h-checkbox entertosend msg-checkbox">
                                                   <input type="checkbox" id="test1">
                                                   <label>&nbsp;</label>
                                                </div>
                                             </div>
                                             <div class="msgdetail-box">
                                                <div class="descholder">
                                                   <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                   <span class="gift-img">
                                                   <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                   <i class="mdi mdi-gift"></i>
                                                   </a>
                                                   </span>
                                                   <img class="msg-status" src="images/msg-read.png">
                                                </div>
                                             </div>
                                          </li>
                                          <li class="msgli msg-outgoing">
                                             <div class="checkbox-holder">
                                                <div class="h-checkbox entertosend msg-checkbox">
                                                   <input type="checkbox" id="test1">
                                                   <label>&nbsp;</label>
                                                </div>
                                             </div>
                                             <div class="msgdetail-box">
                                                <div class="descholder">
                                                   <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                   <span class="gift-img">
                                                   <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                   <i class="mdi mdi-gift"></i>
                                                   </a>
                                                   </span>
                                                   <img class="msg-status" src="images/msg-unread.png">
                                                </div>
                                             </div>
                                          </li>
                                          <li class="msgli msg-outgoing">
                                             <div class="checkbox-holder">
                                                <div class="h-checkbox entertosend msg-checkbox">
                                                   <input type="checkbox" id="test1">
                                                   <label>&nbsp;</label>
                                                </div>
                                             </div>
                                             <div class="msgdetail-box">
                                                <div class="descholder">
                                                   <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                   <span class="gift-img">
                                                   <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                   <i class="mdi mdi-gift"></i>
                                                   </a>
                                                   </span>
                                                   <img class="msg-status" src="images/msg-sent.png">
                                                </div>
                                             </div>
                                          <li class="msgli msg-outgoing">
                                             <div class="checkbox-holder">
                                                <div class="h-checkbox entertosend msg-checkbox">
                                                   <input id="test1" type="checkbox">
                                                   <label>&nbsp;</label>
                                                </div>
                                             </div>
                                             <div class="msgdetail-box">
                                                <div class="descholder">
                                                   <img class="msg-status" src="images/msg-read.png">
                                                   <span class="msg-img">
                                                      <figure><a href="images/msg1.jpg" data-size="1600x1600" data-med="images/msg1.jpg" data-med-size="1024x1024" data-author="Folkert Gorter"><img src="images/msg1.jpg"></a>
                                                         <a href="javascript:void(0)" class="download"><i class="mdi mdi-download"></i></a>
                                                         <span class="timestamp">11:30 PM</span>
                                                      </figure>
                                                      <div class="overlay">
                                                      </div>
                                                   </span>
                                                </div>
                                             </div>
                                          </li>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </li>
                           </ul>
                           <div class="newmessage" id="li-user-blank">
                              <div class="msgdetail-list nice-scroll" tabindex="6"></div>
                           </div>
                           <div class="addnew-msg">
                              <div class="write-msg nice-scroll">
                                 <textarea id="inputMessageWall" class="inputMessageWall materialize-textarea" placeholder="Type your message"></textarea>
                              </div>
                              <div class="msg-stuff">
                                 <div class="send-msg">
                                    <div class="h-checkbox entertosend">
                                       <input id="msgpressentertosend" type="checkbox">
                                       <label>Press enter to send</label>
                                    </div>
                                    <button class="btn btn-primary btn-xxs btn-msg-send" onclick="messageSendFromMessage();"><i class="mdi mdi-telegram"></i></button>
                                 </div>
                                 <div class="add-extra">
                                    <ul>
                                       <li class="messagegiftselection dis-block"><a href="#giftlist-popup" class="popup-modal giftlink"  onclick="manageEmoStickersBox('popular', 'message')">Gift</a></li>
                                       <li>
                                          <div onmouseover="$(this).find('img').attr('src','images/upin-icon-hover.png');" onmouseout="$(this).find('img').attr('src','images/upin-icon.png');">
                                             <div class="custom-file">
                                                <div class="title">
                                                   <a href="javascript:void(0)" onmouseover="$(this).find('img').attr('src','images/upin-icon-hover.png');" onmouseout="$(this).find('img').attr('src','images/upin-icon.png');"><img src="images/upin-icon.png"></a>
                                                </div>
                                                <input id="messageWallFileUpload" name="upload" class="upload" type="file"> 
                                             </div>
                                          </div>
                                       </li>
                                       <li>
                                          <div class="emotion-holder">
                                             <a href="javascript:void(0)" class="emotion-btn" onclick="manageEmotionBox(this,'messages')"><i class="zmdi zmdi-mood"></i></a>
                                             <div class="emotion-box dis-none">
                                                <div class="nice-scroll emotions">
                                                   <ul class="emotion-list">
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                           <div class="bottom-stuff">
                              <h6>Select messages to delete</h6>
                              <div class="btn-holder">
                                 <a href="javascript:void(0)" class="btn btn-primary btn-sm">Delete</a>
                                 <a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="hideMsgCheckbox()">Cancel</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="content-box bshadow giftbox">
            <div class="cbox-title nborder">
               <ul class="nav nav-tabs">
                  <li class="active"><a aria-expanded="false" data-toggle="tab" href="#giftbox-ecard"><span class="iconspan">+</span>eCard</a></li>
                  <li><a aria-expanded="false" data-toggle="tab" href="#giftbox-egift"><span class="iconspan">+</span>eGift</a></li>
                  <li><a aria-expanded="false" data-toggle="tab" href="#giftbox-postcard"><span class="iconspan">+</span>Postcard</a></li>
               </ul>
            </div>
            <div class="cbox-desc">
               <div class="tab-content">
                  <div id="giftbox-ecard" class="tab-pane fade main-pane active in">
                     <div class="vertical-tabs tabs-box">
                        <div class="tabs-list">
                           <ul class="nav nav-tabs">
                              <li class="li-title">Category</li>
                              <li><a href="#ecard-celebration" data-toggle="tab" aria-expanded="false">Celebrations</a></li>
                              <li><a href="#ecard-funny" data-toggle="tab" aria-expanded="false">Funny</a></li>
                              <li><a href="#ecard-lifestyle" data-toggle="tab" aria-expanded="false">Lifystyle</a></li>
                              <li class="active"><a href="#ecard-birthday" data-toggle="tab" aria-expanded="true">Happy Birthday</a></li>
                              <li><a href="#ecard-travel" data-toggle="tab" aria-expanded="false">Travel</a></li>
                              <li><a href="#ecard-seasonal" data-toggle="tab" aria-expanded="false">Seasonal</a></li>
                           </ul>
                        </div>
                        <div class="tabs-detail">
                           <div class="tab-content">
                              <div class="tab-pane fade" id="ecard-celebration">
                                 <div class="choosegift-box">
                                    <ul>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-1.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-2.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-3.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-4.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-5.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-6.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-7.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-8.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-9.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-12.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-13.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-14.png"/></a></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tab-pane fade" id="ecard-funny">
                                 <div class="choosegift-box">
                                    <ul>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-9.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-10.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-11.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-12.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-13.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-14.png"/></a></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tab-pane fade" id="ecard-lifestyle">
                                 <div class="choosegift-box">
                                    <ul>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-1.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-2.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-3.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-4.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-5.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-6.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-7.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-11.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-12.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-13.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-14.png"/></a></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tab-pane fade in active" id="ecard-birthday">
                                 <div class="choosegift-box">
                                    <ul>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-1.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-2.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-3.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-4.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-5.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-6.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-7.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-8.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-9.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-10.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-11.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-12.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-13.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-14.png"/></a></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tab-pane fade" id="ecard-travel">
                                 <div class="choosegift-box">
                                    <ul>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-1.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-2.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-3.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-4.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-5.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-6.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-7.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-8.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-14.png"/></a></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tab-pane fade" id="ecard-seasonal">
                                 <div class="choosegift-box">
                                    <ul>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-1.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-2.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-13.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-14.png"/></a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div id="giftbox-egift" class="tab-pane fade main-pane">
                     <div class="vertical-tabs tabs-box">
                        <div class="tabs-list">
                           <ul class="nav nav-tabs">
                              <li class="li-title">Category</li>
                              <li><a href="#ecard-celebration" data-toggle="tab" aria-expanded="false">Celebrations</a></li>
                              <li><a href="#ecard-funny" data-toggle="tab" aria-expanded="false">Funny</a></li>
                              <li><a href="#ecard-lifestyle" data-toggle="tab" aria-expanded="false">Lify</a></li>
                              <li class="active"><a href="#ecard-birthday" data-toggle="tab" aria-expanded="true">Happy Birthday</a></li>
                              <li><a href="#ecard-travel" data-toggle="tab" aria-expanded="false">Travel</a></li>
                              <li><a href="#ecard-seasonal" data-toggle="tab" aria-expanded="false">Seasonal</a></li>
                           </ul>
                        </div>
                        <div class="tabs-detail">
                           <div class="tab-content">
                              <div class="tab-pane fade" id="ecard-celebration">
                                 <div class="choosegift-box">
                                    <ul>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-1.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-2.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-3.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-4.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-5.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-6.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-7.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-8.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-9.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-12.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-13.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-14.png"/></a></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tab-pane fade" id="ecard-funny">
                                 <div class="choosegift-box">
                                    <ul>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-9.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-10.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-11.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-12.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-13.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-14.png"/></a></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tab-pane fade" id="ecard-lifestyle">
                                 <div class="choosegift-box">
                                    <ul>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-1.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-2.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-3.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-4.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-5.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-6.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-7.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-11.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-12.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-13.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-14.png"/></a></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tab-pane fade in active" id="ecard-birthday">
                                 <div class="choosegift-box">
                                    <ul>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-1.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-2.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-3.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-4.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-5.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-6.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-7.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-8.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-9.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-10.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-11.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-12.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-13.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-14.png"/></a></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tab-pane fade" id="ecard-travel">
                                 <div class="choosegift-box">
                                    <ul>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-1.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-2.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-3.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-4.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-5.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-6.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-7.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-8.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-14.png"/></a></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tab-pane fade" id="ecard-seasonal">
                                 <div class="choosegift-box">
                                    <ul>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-1.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-2.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-13.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-14.png"/></a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div id="giftbox-postcard" class="tab-pane fade main-pane">
                     <div class="vertical-tabs tabs-box">
                        <div class="tabs-list">
                           <ul class="nav nav-tabs">
                              <li class="li-title">Category</li>
                              <li><a href="#ecard-celebration" data-toggle="tab" aria-expanded="false">Celebrations</a></li>
                              <li><a href="#ecard-funny" data-toggle="tab" aria-expanded="false">Funny</a></li>
                              <li><a href="#ecard-lifestyle" data-toggle="tab" aria-expanded="false">Lifystyle</a></li>
                              <li class="active"><a href="#ecard-birthday" data-toggle="tab" aria-expanded="true">Happy Birthday</a></li>
                              <li><a href="#ecard-travel" data-toggle="tab" aria-expanded="false">Travel</a></li>
                              <li><a href="#ecard-seasonal" data-toggle="tab" aria-expanded="false">Seasonal</a></li>
                           </ul>
                        </div>
                        <div class="tabs-detail">
                           <div class="tab-content">
                              <div class="tab-pane fade" id="ecard-celebration">
                                 <div class="choosegift-box">
                                    <ul>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-1.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-2.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-3.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-4.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-5.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-6.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-7.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-8.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-9.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-12.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-13.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-14.png"/></a></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tab-pane fade" id="ecard-funny">
                                 <div class="choosegift-box">
                                    <ul>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-9.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-10.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-11.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-12.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-13.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-14.png"/></a></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tab-pane fade" id="ecard-lifestyle">
                                 <div class="choosegift-box">
                                    <ul>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-1.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-2.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-3.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-4.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-5.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-6.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-7.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-11.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-12.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-13.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-14.png"/></a></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tab-pane fade in active" id="ecard-birthday">
                                 <div class="choosegift-box">
                                    <ul>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-1.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-2.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-3.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-4.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-5.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-6.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-7.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-8.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-9.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-10.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-11.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-12.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-13.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-14.png"/></a></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tab-pane fade" id="ecard-travel">
                                 <div class="choosegift-box">
                                    <ul>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-1.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-2.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-3.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-4.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-5.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-6.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-7.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-8.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-14.png"/></a></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tab-pane fade" id="ecard-seasonal">
                                 <div class="choosegift-box">
                                    <ul>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-1.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-2.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-13.png"/></a></li>
                                       <li><a href="javascript:void(0)"><img src="images/ecard-birthday-14.png"/></a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>	

<?php include('common/usegift_popup.php'); ?>
<?php include("script.php"); ?>
</body>
</html>