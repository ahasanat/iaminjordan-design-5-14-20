<div id="privacymodal" class="modal compose_inner_modal modalxii_level1">
    <div class="content_header">
		<button class="close_span waves-effect">
			<i class="mdi mdi-close mdi-20px material_close"></i>
		</button>
		<p class="selected_person_text">Choose privacy</p>
		<a href="javascript:void(0)" id="customdoneprivacymodal" class="done_btn action_btn">Done</a>
    </div>
    <div class="person_box">
		<div class="row">
			<div class="head privatehead">
			<div class="col s2 m2 l2 xl2 col1 valign-wrapper center-align"><i class="mdi mdi-lock mdi-20px center-align"></i></div>
			<div class="col s8 m8 l8 xl8 col2 valign-wrapper">Private</div>
			<div class="col s2 m2 l2 xl2 col3 valign-wrapper center-align">
				<input name="privacyradiomodal" type="radio" id="radio123" value="Private"/>
				<label for="radio123" class="label"></label>
			</div>
			</div>
			<div class="clear"></div>
			
			<div class="head connectionshead">
			<div class="col s2 m2 l2 xl2 col1 valign-wrapper center-align"><i class="mdi mdi-account mdi-20px center-align"></i></div>
			<div class="col s8 m8 l8 xl8 col2 valign-wrapper center-align">Connections</div>
			<div class="col s2 m2 l2 xl2 col3 valign-wrapper center-align">
				<input name="privacyradiomodal" type="radio" id="radio456" value="Connections" />
				<label for="radio456" class="label"></label>
			</div>
			</div>
			<div class="clear"></div>
			
			<div class="head customli_modal customhead">
				<div class="col s2 m2 l2 xl2 col1 valign-wrapper center-align">
					<i class="mdi mdi-settings mdi-20px center-align"></i>
				</div>
				<div class="col s8 m8 l8 xl8 col2 valign-wrapper center-align">
						<div class="col s10 m10 l10 xl10 left-align internal">Custom</div>
						<div class="col s2 m2 l2 xl2 left right-align internal">
							<a href="javascript:void(0)">
								<i class="zmdi zmdi-edit mdi-20px"></i>
							</a>
						</div>
				</div>
				<div class="col s2 m2 l2 xl2 col3 valign-wrapper center-align">
					<input name="privacyradiomodal" type="radio" id="radio012" value="Custom" />
					<label for="radio012" class="label"></label>
				</div>
			</div>
			<div class="clear"></div>
			
			<div class="head publichead">
			<div class="col s2 m2 l2 xl2 col1 valign-wrapper center-align"><i class="mdi mdi-earth mdi-20px center-align"></i></div>
			<div class="col s8 m8 l8 xl8 col2 valign-wrapper center-align">Public</div>
			<div class="col s2 m2 l2 xl2 col3 valign-wrapper center-align">
				<input name="privacyradiomodal" type="radio" id="radio1233" value="Public" />
				<label for="radio1233" class="label"></label>
			</div>
			</div>
			<div class="clear"></div>

			<div class="uniquehead">
			<div class="col s2 m2 l2 xl2 col1 valign-wrapper center-align"><i class="mdi mdi-check-circle-outline mdi-20px center-align"></i></div>
			<div class="col s8 m8 l8 xl8 col2 valign-wrapper center-align">Enabled</div>
			<div class="col s2 m2 l2 xl2 col3 valign-wrapper center-align">
				<input name="privacyradiomodal" type="radio" id="radio1233I" value="Enabled" />
				<label for="radio1233I" class="label"></label>
			</div>
			</div>
			<div class="clear"></div>
			
			<div class="uniquehead">
			<div class="col s2 m2 l2 xl2 col1 valign-wrapper center-align"><i class="mdi mdi-minus-circle-outline mdi-20px center-align"></i></div>
			<div class="col s8 m8 l8 xl8 col2 valign-wrapper center-align">Disabled</div>
			<div class="col s2 m2 l2 xl2 col3 valign-wrapper center-align">
				<input name="privacyradiomodal" type="radio" id="radio1233II" value="Disabled" />
				<label for="radio1233II" class="label"></label>
			</div>
			</div>

		</div>
    </div>
</div>
