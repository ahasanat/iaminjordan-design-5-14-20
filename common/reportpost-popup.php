<div id="reportpost-popup" class="modal reportabuse_modal compose_tool_box post-popup custom_modal main_modal">
   <div class="hidden_header">
      <div class="content_header">
         <button class="close_span cancel_poup waves-effect">
         <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
         </button>
         <p class="modal_header_xs">Report abuse</p>
         <a type="button" class="post_btn action_btn post_btn_xs close_modal waves-effect" onclick="verify()">Report</a>
      </div>
   </div>
   <div class="modal-content">
      <div class="new-post active">
         <div class="top-stuff">
            <!--<div class="side-user">-->
            <div class="postuser-info">
               <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
               <div class="desc-holder">
                  <span class="profile_name"><a href="javascript:void(0)">Nimish Parekh</a></span>							
               </div>
            </div>
         </div>
         <div class="clear"></div>
         <div class="scroll_div">
            <div class="npost-content">
               <div class="post-mcontent">
                  <div class="desc">
                     <!--<textarea class="newpost-tt materialize-textarea" placeholder="What's new?"></textarea>-->
                     <textarea id="new_post_comment" placeholder="Reason for reporting" class="materialize-textarea comment_textarea new_post_comment"></textarea>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="modal-footer">
      <div class="new-post active">
         <div class="post-bcontent">
            <div class="post-bholder">
               <div class="post-loader"><img src="images/home-loader.gif"/></div>
               <div class="hidden_xs">
                  <a class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
                  <a type="button" class="btngen-center-align waves-effect">Report</a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>