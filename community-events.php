<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div>
<div class="container page_container event_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout ipad-mfix">
      <div class="main-content with-lmenu commevents-page main-page grid-view general-page">
         <div class="combined-column combined_md_column">
            <div class="content-box nbg">
               <div class="cbox-desc md_card_tab">
                  <div class="fake-title-area divided-nav mobile-header">
                     <ul class="tabs">
                        <li class="tab col s3">
                           <a class="active" href="#commevents-suggested" data-toggle="tab" aria-expanded="true" onclick="gridBoxImgUINew('.gridBox127');">Suggested</a>
                        </li>
                        <li class="tab col s3">
                           <a href="#commevents-attending" data-toggle="tab" aria-expanded="false" onclick="gridBoxImgUINew('.gridBox127');">Attending</a>
                        </li>
                        <li class="tab col s3">
                           <a href="#commevents-yours" data-toggle="tab" aria-expanded="false" onclick="gridBoxImgUINew('.gridBox127');">Yours</a>
                        </li>
                     </ul> 
                  </div>
                  <div class="tab-content view-holder grid-view">
                     <div class="tab-pane fade main-pane active in" id="commevents-suggested">
                        <div class="commevents-list generalbox-list">
                           <div class="clear"></div>
                           <div class="row">
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable eventCard animated fadeInUp">
                                    <a href="community-events-detail.php" class="commevents-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-commevents.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>CS London weekly meet #234 @Pendrel's Oak, Holborn</h4>
                                          <div class="icon-line">
                                             <span>High holborn, London WC124VP, UK</span>
                                          </div>
                                          <div class="userinfo">
                                             <span class="month">Nov</span>
                                             <span class="date">17</span>
                                          </div>
                                          <div class="username">
                                             <span>Greg Batmarx</span>
                                          </div>
                                          <div class="icon-line subtext">														
                                             5 Attending
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">5 Attending</span>
                                             <span class="noClick" onclick="commeventsGoing(event,'commevent1',this)">Attend</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable eventCard animated fadeInUp">
                                    <a href="community-events-detail.php" class="commevents-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-2.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Renewable energy</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <span class="month">Nov</span>
                                             <span class="date">17</span>
                                          </div>
                                          <div class="username">
                                             <span>Greg Batmarx</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick" onclick="commeventsGoing(event,'commevent2',this)">Attend</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable eventCard animated fadeInUp">
                                    <a href="community-events-detail.php" class="commevents-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-3.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Black-white illustrations to H. L.</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <span class="month">Nov</span>
                                             <span class="date">17</span>
                                          </div>
                                          <div class="username">
                                             <span>Henry Lion Oldie</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick" onclick="commeventsGoing(event,'commevent3',this)">Attend</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable eventCard animated fadeInUp">
                                    <a href="community-events-detail.php" class="commevents-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-4.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>B&W Photography</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <span class="month">Nov</span>
                                             <span class="date">17</span>
                                          </div>
                                          <div class="username">
                                             <span>Rui Luis</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick" onclick="commeventsGoing(event,'commevent4',this)">Attend</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable eventCard animated fadeInUp">
                                    <a href="community-events-detail.php" class="commevents-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-5.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Light in Motion</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <span class="month">Nov</span>
                                             <span class="date">17</span>
                                          </div>
                                          <div class="username">
                                             <span>Alex Lapidus</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick" onclick="commeventsGoing(event,'commevent5',this)">Attend</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable eventCard animated fadeInUp">
                                    <a href="community-events-detail.php" class="commevents-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-6.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>India</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <span class="month">Nov</span>
                                             <span class="date">17</span>
                                          </div>
                                          <div class="username">
                                             <span>Suzanne Bell</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick" onclick="commeventsGoing(event,'commevent6',this)">Attend</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable eventCard animated fadeInUp">
                                    <a href="community-events-detail.php" class="commevents-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-7.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>My Feathered Connections</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <span class="month">Nov</span>
                                             <span class="date">17</span>
                                          </div>
                                          <div class="username">
                                             <span>Chwee Hock Low</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick" onclick="commeventsGoing(event,'commevent7',this)">Attend</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade main-pane dis-none" id="commevents-attending">								
                     </div>
                     <div class="tab-pane fade main-pane commevents-yours general-yours dis-none" id="commevents-yours">
                        <div class="commevents-list generalbox-list">
                           <div class="row">
                              <div class="col s6 m4 l3 gridBox127 add-cbox">
                                 <div class="card hoverable eventCard animated fadeInUp">
                                    <div class="general-box">
                                       <a href="javascript:void(0)" class="add-commevents add-general" onclick="openAddItemModal()">
                                       <span class="icont">+</span>
                                       Create Community Event
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable eventCard animated fadeInUp">
                                    <a href="community-events-detail.php" class="commevents-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-1.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>CS London weekly meet #234 @Pendrel's Oak, Holborn</h4>
                                          <div class="icon-line">
                                             <span>High holborn, London WC124VP, UK</span>
                                          </div>
                                          <div class="userinfo">
                                             <span class="month">Nov</span>
                                             <span class="date">17</span>
                                          </div>
                                          <div class="username">
                                             <i class="mdi mdi-calendar"></i>
                                             <span>Every Tuesday at 5:30 PM</span>
                                          </div>
                                          <div class="icon-line subtext">
                                             5 Attending
                                          </div>
                                          <div class="action-btns">														
                                             <span class="followers">5 Attending</span>
                                             <span class="noClick" onclick="commeventsGoing(event,'commevent1',this)">Attend</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable eventCard animated fadeInUp">
                                    <a href="community-events-detail.php" class="commevents-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-2.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Renewable energy</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <span class="month">Nov</span>
                                             <span class="date">17</span>
                                          </div>
                                          <div class="username">
                                             <span>Greg Batmarx</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span onclick="commeventsGoing(event,'commevents2',this)">Attend</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable eventCard animated fadeInUp">
                                    <a href="community-events-detail.php" class="commevents-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-3.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Black-white illustrations to H. L.</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <span class="month">Nov</span>
                                             <span class="date">17</span>
                                          </div>
                                          <div class="username">
                                             <span>Henry Lion Oldie</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span onclick="commeventsGoing(event,'commevents3',this)">Attend</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable eventCard animated fadeInUp">
                                    <a href="community-events-detail.php" class="commevents-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-4.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>B&W Photography</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <span class="month">Nov</span>
                                             <span class="date">17</span>
                                          </div>
                                          <div class="username">
                                             <span>Rui Luis</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span onclick="commeventsGoing(event,'commevents4',this)">Attend</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable eventCard animated fadeInUp">
                                    <a href="community-events-detail.php" class="commevents-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-5.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Light in Motion</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <span class="month">Nov</span>
                                             <span class="date">17</span>
                                          </div>
                                          <div class="username">
                                             <span>Alex Lapidus</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span onclick="commeventsGoing(event,'commevents5',this)">Attend</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable eventCard animated fadeInUp">
                                    <a href="community-events-detail.php" class="commevents-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-6.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>India</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <span class="month">Nov</span>
                                             <span class="date">17</span>
                                          </div>
                                          <div class="username">
                                             <span>Suzanne Bell</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span onclick="commeventsGoing(event,'commevents6',this)">Attend</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable eventCard animated fadeInUp">
                                    <a href="community-events-detail.php" class="commevents-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-7.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>My Feathered Connections</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <span class="month">Nov</span>
                                             <span class="date">17</span>
                                          </div>
                                          <div class="username">
                                             <span>Chwee Hock Low</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span onclick="commeventsGoing(event,'commevents7',this)">Attend</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php include('common/chat.php'); ?>
      </div>
   </div>
</div>

<?php include("common/footer.php"); ?>
</div>	

<?php include('common/upload_img_box.php'); ?>


<!--add event modal-->
<div id="add_event_modal" class="modal add-item-popup custom_md_modal">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>	
            <h3>Create Event</h3>
            <a type="button" class="item_done crop_done hidden_close_span close_modal waves-effect" href="javascript:void(0)" >Done</a>
         </div>
         <div class="main-pcontent">
            <form class="add-item-form">
               <div class="frow frowfull">
                  <div class="crop-holder" id="image-cropper">
                     <div class="cropit-preview"></div>
                     <div class="main-img">
                        <img src="images/additem-commevents.png">
                     </div>
                     <div class="main-img1">
                        <img id="imageid" draggable="false"/>
                     </div>
                     <div class="btnupload custom_up_load" id="upload_img_action">
                        <div class="fileUpload">
                           <i class="zmdi zmdi-camera zmdi-hc-lg"></i>
                           <input type="file" name="filupload" id="crop-file" class="upload cropit-image-input" />
                        </div>
                     </div>
                     <a  href="javascript:void(0)" class="btn btn-save image_save_btn image_save dis-none">
                     <span class="zmdi zmdi-check"></span>
                     </a>
                     <a id="removeimg" href="javascript:void(0)" class="collection_image_trash image_trash">
                     <i class="mdi mdi-close"></i>
                     </a>
                  </div>
               </div>
               <div class="sidepad">
                  <div class="frow">
                     <input id="event_title" type="text" class="validate item_title" placeholder="Event title" />
                  </div>
                  <div class="frow">
                     <textarea id="Collection_tagline" class="materialize-textarea mb0 md_textarea item_tagline" placeholder="Event tagline"></textarea>
                     <span class="char-limit">0/80</span>								
                  </div>
                  <div class="frow">
                     <input type="text" placeholder="Address" data-query="all" onfocus="filderMapLocationModal(this)" class="materialize-textarea md_textarea item_address" autocomplete="off"/>
                  </div>
                  <div class="frow">
                     <div class="row">
                        <div class="col s6">
                           <div class="input-field dateinput">
                              <div class="date_picker_container">
                                 <input type="text" data-toggle="datepicker"  value="2018-10-03" class="datepickerinput" data-query="all" placeholder="Date" readonly/>
                              </div>
                           </div>
                        </div>
                        <div class="col s6"> 
                           <div class="input-field dateinput">
                              <div class="date_picker_container">
                                 <input type="text" class="time timepicker"  placeholder="Time"/>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="frow">
                     <textarea type="text" placeholder="Tell people more about the event" class="materialize-textarea md_textarea item_about"></textarea>
                  </div>
                  <div class="frow security-area">
                     <label>Privacy</label>
                     <div class="right">
                        <a class="dropdown_text dropdown-button" href="javascript:void(0)" data-activates="collection_privacy">
                        <span>Public</span>
                        <i class="mdi mdi-menu-down"></i>
                        </a>
                        <ul id="collection_privacy" class="dropdown-content">
                           <li> <a href="javascript:void(0)">Private</a> </li>
                           <li> <a href="javascript:void(0)">Connections</a> </li> 
                           <li class="customli_modal"> <a href="javascript:void(0)">Custom</a> </li> 
                           <li> <a href="javascript:void(0)">Public</a> </li>
                        </ul>
                     </div>
                  </div>
                  <div class="frow">
                     <div class="expandable-holder">
                        <a href="javascript:void(0)" class="expand-link invertsign" onclick="mng_expandable(this)">Advanced Option <i class="mdi mdi-menu-down"></i></a>
                        <div class="expandable-area">
                           <div class="frow">
                              <input type="text" placeholder="Website URL (optional)" class="materialize-textarea md_textarea item_website"/>
                           </div>
                           <div class="frow">
                              <input type="text" placeholder="Ticket seller URL (optional)" class="materialize-textarea md_textarea item_ticket"/>
                           </div>
                           <div class="frow">
                              <input type="text" placeholder="Youtube URL (optional)" class="materialize-textarea md_textarea item_youtube"/>
                           </div>
                           <div class="frow">
                              <input type="text" placeholder="Transit and parking information (optional)" class="materialize-textarea md_textarea item_parking"/>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect" data-class="addbtn">Create</a>
   </div>
</div>

<?php include('common/custom_modal.php'); ?>

<div class="community-event-datepicker">
   <?php include('common/datepicker.php'); ?>
</div>

<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
   <?php include('common/map_modal.php'); ?>
</div>

<?php include('common/discard_popup.php'); ?>
<?php include("script.php"); ?>	
</body>
</html>